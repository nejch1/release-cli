package gitlab

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestClient_request(t *testing.T) {
	tests := []struct {
		name       string
		method     string
		baseURL    string
		token      string
		projectID  string
		wantErrMsg string
	}{
		{
			name:      "no_error",
			baseURL:   "http://127.0.0.1",
			token:     "token",
			projectID: "projectID",
		},
		{
			name:       "invalid_url",
			baseURL:    "%",
			wantErrMsg: "invalid URL escape \"%\"",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gc, err := New(tt.baseURL, tt.token, tt.projectID, &MockHTTPClient{})
			if tt.wantErrMsg != "" {
				require.NotNil(t, err)
				require.Contains(t, err.Error(), tt.wantErrMsg)
				return
			}
			require.NoError(t, err)

			got, err := gc.request(context.Background(), tt.method, tt.baseURL, nil)
			require.NoError(t, err)

			require.NotNil(t, got)
			require.Equal(t, got.Header.Get("JOB-TOKEN"), tt.token)
			require.Equal(t, got.Header.Get("Content-Type"), "application/json")
		})
	}
}
